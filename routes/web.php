<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\MovieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

// Route::group(['middleware' => ['auth']], function () {
// });
Route::group(['middleware' => ['verified'], 'verify' => true], function () {

    Route::prefix('dashboard')->group(function () {
        //admin pages

        Route::get('/', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');
        Route::get('book/{title}', ['as' => 'book.show', 'uses' =>   'BookController@show']);
        Route::resource('roles', RoleController::class);
        Route::resource('users', UserController::class);
        Route::resource('genre', GenreController::class);
        Route::resource('movie', MovieController::class);
    });
});
require __DIR__ . '/auth.php';
