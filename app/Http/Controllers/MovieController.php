<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\Genre;
use Illuminate\Http\Request;

class MovieController extends Controller
{


    function __construct()
    {
        $this->middleware('permission:movie-list|movie-create|movie-edit|movie-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:movie-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:movie-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:movie-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::latest()->paginate(5);
        return view('dashboard.movie.index', compact('movies'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = Genre::all();
        return view('dashboard.movie.create', compact('genres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        request()->validate([

            'title' => 'required',
            'genre' => 'required',
            'duration' => 'required',
            'actor_id' => 'required',
            'sinopsis' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'trailer' => 'required|file|mimetypes:video/mp4',
            'rating' => 'required',

        ]);

        $posterName = time() . '.' . $request->poster->extension();
        $request->poster->move(public_path('images'), $posterName);


        $trailerName = time() . '.' . $request->trailer->extension();
        $request->trailer->move(public_path('videos'), $trailerName);



        $movie = Movie::create([


            'title' => $request->input('title'),
            'genre' => json_encode($request->input('genre')),
            'duration' => $request->input('duration'),
            'actor_id' => $request->input('actor_id'),
            'sinopsis' => $request->input('sinopsis'),
            'poster'       =>  $posterName,
            'trailer' => $trailerName,
            'rating' => $request->input('rating'),
        ]);


        $movie->save();


        return redirect()->route('movie.index')
            ->with('success', 'Movie created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        return view('dashboard.movie.show', compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        $genres = Genre::all();
        return view('dashboard.movie.edit', compact('movie', 'genres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        request()->validate([
            'title' => 'required',
            'genre_id' => 'required',
            'duration' => 'required',
            'actor_id' => 'required',
            'sinopsis' => 'required',
            'poster' => 'required',
            'trailer' => 'required',
            'rating' => 'required',
        ]);

        $movie->update($request->all());

        return redirect()->route('movie.index')
            ->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        $movie->delete();

        return redirect()->route('movie.index')
            ->with('success', 'Movie deleted successfully');
    }
}
