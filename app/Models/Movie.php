<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'genre',
        'duration',
        'actor_id',
        'sinopsis',
        'poster',
        'trailer',
        'rating',

    ];

    public function setCategoryAttribute($value)
    {
        $this->attributes['genre'] = json_encode($value);
    }

    public function getCategoryAttribute($value)
    {
        return $this->attributes['genre'] = json_decode($value);
    }
}
