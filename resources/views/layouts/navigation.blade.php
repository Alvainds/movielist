{{-- <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar  collapse">
    <div class="position-sticky pt-3">
        <ul class="nav flex-column">

            <li class="nav-item">
                <a href="{{ route('home') }}" :active="request()->routeIs('home')" class="nav-link active"
                    aria-current="page">
                    <span data-feather="home" class="align-text-bottom"></span>
                    Home
                </a>
            </li>



            <li class="nav-item">
                <a class="nav-link" href="{{ route('dashboard') }}" :active="request()->routeIs('dashboard')">
                    <span data-feather="file" class="align-text-bottom"></span>
                    Dashboard
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('users.index') }}" :active="request()->routeIs('users.index')">
                    <span data-feather="shopping-cart" class="align-text-bottom"></span>
                    Manage Users
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('movie.index') }}" :active="request()->routeIs('movie.index')">
                    <span data-feather="users" class="align-text-bottom"></span>
                    Manage Movie
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('roles.index') }}" :active="request()->routeIs('roles.index')">
                    <span data-feather="bar-chart-2" class="align-text-bottom"></span>
                    Manage Role
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('genre.index') }}" :active="request()->routeIs('genre.index')">
                    <span data-feather="bar-chart-2" class="align-text-bottom"></span>
                    Genre
                </a>
            </li>


        </ul>


    </div>
</nav> --}}

<header class="topbar bg-dark">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark ">
        <div class="navbar-header">
            <!-- This is for the sidebar toggle which is visible on mobile only -->
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                    class="ti-menu ti-close pr-2"></i></a>
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <a class="navbar-brand bg-dark" href="index.html">
                <!-- Logo icon -->
                <b class="logo-icon">
                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span class="logo-text">
                    MovieList
                </span>
            </a>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Toggle which is visible on mobile only -->
            <!-- ============================================================== -->
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more pr-2"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse bg-dark collapse" id="navbarSupportedContent">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light"
                        href="javascript:void(0)" data-sidebartype="mini-sidebar"><i
                            class="icon-arrow-left-circle pr-2"></i></a></li>



            </ul>
            <!-- ============================================================== -->
            <!-- Right side toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav ">
                <!-- ============================================================== -->
                <!-- Search -->
                <!-- ============================================================== -->
                <li class="nav-item search-box d-none d-md-block">
                    <form class="app-search mt-3 mr-2">
                        <input type="text" class="form-control rounded-pill border-0" placeholder="Search for...">
                        <a class="srh-btn"><i class="ti-search pr-2"></i></a>
                    </form>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href=""
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                            class="bi bi-person-circle text-light fs-3"></i></a>
                    <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                        <div class="d-flex no-block align-items-center p-3 mb-2 border-bottom">
                            <div class=""><img src="../assets/images/users/1.jpg" alt="user" class="rounded" width="80">
                            </div>
                            <div class="ml-2">
                                <h4 class="mb-0">Steave Jobs</h4>
                                <p class=" mb-0">varun@gmail.com</p>
                                <a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a>
                            </div>
                        </div>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user mr-1 ml-1 pr-2"></i> My
                            Profile</a>
                        <a class="dropdown-item" href="javascript:void(0)">
                            <i class="ti-wallet mr-1 ml-1 pr-2"></i> My
                        </a>
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <a class="dropdown-item" onclick="event.preventDefault();
                                                                            this.closest('form').submit();"
                                href="{{ route('logout') }}">
                                <i class="ti-wallet mr-1 ml-1 pr-2"></i> Logout
                            </a>

                        </form>

                    </div>
                </li>

                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    </a>
                    <div class="dropdown-menu dropdown-menu-right  animated bounceInDown"
                        aria-labelledby="navbarDropdown2">

                    </div>
                </div>
            </ul>
        </div>
    </nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">



                <li class="sidebar-item">
                    <a :active="request()->routeIs('home')"
                        class="sidebar-link text-decoration-none waves-effect waves-dark sidebar-link text-decoration-none"
                        href="{{ route('home') }}" aria-expanded="false">
                        <i class="bi bi-house-door  pr-2"></i>
                        <span class="hide-menu">Home</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a :active="request()->routeIs('dashboard')"
                        class="sidebar-link text-decoration-none waves-effect waves-dark sidebar-link text-decoration-none"
                        href="{{ route('dashboard') }}" aria-expanded="false">
                        <i class="bi bi-speedometer2 pr-2"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a :active="request()->routeIs('users.index')"
                        class="sidebar-link text-decoration-none waves-effect waves-dark sidebar-link text-decoration-none"
                        href="{{ route('users.index') }}" aria-expanded="false">
                        <i class="bi bi-people pr-2"></i>
                        <span class="hide-menu">Users</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a :active="request()->routeIs('movie.index')" class="sidebar-link text-decoration-none waves-effect waves-dark sidebar-link
                        text-decoration-none" href="{{ route('movie.index') }}" aria-expanded="false">
                        <i class="bi bi-camera-reels pr-2"></i>
                        <span class="hide-menu">Movie</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a :active="request()->routeIs('roles.index')" class="sidebar-link text-decoration-none waves-effect waves-dark sidebar-link
                                            text-decoration-none" href="{{ route('roles.index') }}"
                        aria-expanded="false">
                        <i class="bi bi-list-task pr-2"></i>
                        <span class="hide-menu">Role</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a :active="request()->routeIs('genre.index')" class="sidebar-link text-decoration-none waves-effect waves-dark sidebar-link
                                                            text-decoration-none" href="{{ route('genre.index') }}"
                        aria-expanded="false">
                        <i class="bi bi-bookmark pr-2"></i>
                        <span class="hide-menu">Genre</span>
                    </a>
                </li>


            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>

</aside>