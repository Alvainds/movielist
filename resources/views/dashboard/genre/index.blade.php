<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users') }}
        </h2>
    </x-slot>



    <div class="row mb-3">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Movie</h2>
            </div>
            <div class="pull-right">
                @can('movie-create')
                <a class="btn btn-success" href="{{ route('genre.create') }}"> Create New Genres</a>
                @endcan
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Description</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($genres as $genre)
        <tr>
            <td></td>
            <td>{{ $genre->name }}</td>
            <td>{{ $genre->description }}</td>

            <td>
                <form action="{{ route('genre.destroy',$genre->id) }}" method="POST">
                    <a class="btn btn-info" href="{{ route('genre.show',$genre->id) }}">Show</a>
                    {{-- @can('genre-edit') --}}
                    <a class="btn btn-primary" href="{{ route('genre.edit',$genre->id) }}">Edit</a>
                    {{-- @endcan --}}


                    @csrf
                    @method('DELETE')
                    {{-- @can('genre-delete') --}}
                    <button type="submit" class="btn btn-danger">Delete</button>
                    {{-- @endcan --}}
                </form>
            </td>
        </tr>
        @endforeach
    </table>


</x-app-layout>