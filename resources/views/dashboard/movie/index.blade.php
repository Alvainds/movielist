<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users') }}
        </h2>
    </x-slot>



    <div class="row mb-3">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                @can('movie-create')
                <a class="btn btn-success" href="{{ route('movie.create') }}"> Create New Movie</a>
                @endcan
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Poster</th>
            <th>information</th>

            <th width="280px">Action</th>
        </tr>
        @foreach ($movies as $movie)
        <tr>
            <td style="width: 1px">{{ ++$i }}</td>
            <td style="width: 125px"><img style="width: 125px;object-fit:cover;height:190px;"
                    src="{{ asset('images/'.$movie->poster)  }}" alt="" srcset=""></td>
            <td>
                <h6>
                    <a class="text-decoration-none text-dark" href="{{ route('movie.show',$movie->id) }}">{{
                        $movie->title }}</a>
                </h6>
                @foreach (json_decode($movie->genre) as $genre)
                <a class="text-decoration-none text-muted" href=""> {{ $genre }}</a>
                @endforeach
                <p><i class="bi bi-star-fill"></i> {{ $movie->rating }}</p>

                <p>{{ $movie->sinopsis }}</p>

            </td>

            <td>
                <form action="{{ route('movie.destroy',$movie->id) }}" method="POST">

                    @can('movie-edit')
                    <a class="btn btn-primary" href="{{ route('movie.edit',$movie->id) }}">Edit</a>
                    @endcan


                    @csrf
                    @method('DELETE')
                    @can('movie-delete')
                    <button type="submit" class="btn btn-danger">Delete</button>
                    @endcan
                </form>
            </td>
        </tr>
        @endforeach
    </table>


    {!! $movies->links() !!}
</x-app-layout>