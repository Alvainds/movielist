<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Movie') }}
        </h2>
    </x-slot>



    <div class="row mb-3">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Movie</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('movie.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


    <form action="{{ route('movie.store') }}" method="POST" enctype="multipart/form-data">
        @csrf


        <div class="row">
            <div class="mb-3">
                <label class="form-label">Title</label>
                <input value="{{ $movie->title }}" type="text" name="title" class="form-control">
            </div>
            <img class="mb-3" style="width: 200px" src="{{asset('images/'.$movie->poster) }}" alt="" srcset="">
            <div class="mb-3">
                <label for="formFile" class="form-label">Poster</label>
                <input name="poster" class="form-control" type="file" id="formFile">
            </div>
            <div class="mb-3">
                <video controls>
                    <source src="{{ asset('videos/'.$movie->trailer) }}" />
                </video>
            </div>
            <div class="mb-3">
                <label for="formFile" class="form-label">Trailer</label>
                <input name="trailer" class="form-control" type="file" id="formFile">
            </div>
            <div class="mb-3">
                <label for="customRange2" class="form-label">Rating</label>
                <input name="rating" type="range" class="form-range" min="0" max="5" id="customRange2">
            </div>

            <div class="mb-3">
                @foreach($genres as $genre)
                <div class="form-check">
                    <input class="form-check-input" name="genre[]" type="checkbox" value="{!! $genre->name !!}"
                        id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                        {{$genre->name}}
                    </label>

                </div>
                @endforeach
            </div>
            <div class="mb-3">
                <label class="form-label">Duration</label>
                <input type="time" name="duration" class="form-control">
            </div>

            <div class="mb-3">
                <label class="form-label">Actor</label>
                <select name="actor_id" class="form-select" aria-label="Default select example">
                    <option selected>Open this select menu</option>
                    <option value="1">Somebody</option>
                    <option value="2">Comedy</option>
                    <option value="3">Three</option>
                </select>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group mb-3">
                    <label class="form-label">Sinopsis</label>
                    <textarea class="form-control" style="height:150px" name="sinopsis" placeholder="Detail"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>


    </form>
</x-app-layout>