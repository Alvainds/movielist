<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>


    <h3>{{ $movie->title }}</h3>
    <p>{{ $movie->duration }}</p>

    <div class="row">
        <div class="col-3">
            <div class="card">
                <img style="height: 467px" src="{{asset('images/'.$movie->poster) }}" alt="" srcset="">
            </div>
        </div>
        <div class="col-9">
            <div class="card">
                <video controls>
                    <source src="{{ asset('videos/'.$movie->trailer) }}" />
                </video>
            </div>
        </div>
    </div>
    </div>



    <div class="row mb-3">
        <div class="col">
            @foreach (json_decode($movie->genre) as $genre)
            <button type="button" class="btn btn-outline-dark mx-1 rounded-pill">{{ $genre }}</button>
            @endforeach
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <p>{{ $movie->sinopsis }}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <h6>Actor {{ $movie->actor_id }}</h6>
        </div>
    </div>


</x-app-layout>